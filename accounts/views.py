from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import PasswordChangeView, LogoutView, LoginView
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy, reverse
from django.views.generic import UpdateView, DeleteView, CreateView, ListView

from accounts.forms import AccountRegistrationForm, AccountUpdateForm
from accounts.models import User


class UsersListView(LoginRequiredMixin, ListView):
    model = User
    template_name = 'leaderboard.html'
    context_object_name = 'users'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['users'] = User.objects.exclude(rating=0).order_by('-rating')
        return context


class AccountPasswordChangeView(PasswordChangeView):
    model = User
    template_name = 'password_change.html'
    success_url = reverse_lazy('index')


class AccountRegistrationView(CreateView):
    model = User
    template_name = 'registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'User successfully registered')
        return result


class AccountLoginView(LoginView):
    success_url = reverse_lazy('index')
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('index')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f'User {self.request.user} successfully logged in')
        return result


class AccountUpdateView(UpdateView):
    model = User
    template_name = 'profile.html'
    success_url = reverse_lazy('index')
    form_class = AccountUpdateForm

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f" Profile: User {self.request.user} has succesfully updated")
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            messages.info(self.request, f'User {self.request.user} has been logged out')
        return super().dispatch(request, *args, **kwargs)


class AccountDeleteView(DeleteView):
    def get(self, request, *args, **kwargs):
        user = self.request.user

        return render(request,
                      template_name='profile:delete.html',
                      context={'user': user}
                      )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        user.delete()

        return render(request,
                      template_name='index.html',
                      context={'user': user}
                      )
